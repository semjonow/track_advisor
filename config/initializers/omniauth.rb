Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, OMNIAUTH_FACEBOOK_APP_ID, OMNIAUTH_FACEBOOK_APP_SECRET, :scope => "email,publish_actions,offline_access"
  provider :twitter,  OMNIAUTH_TWITTER_APP_ID,  OMNIAUTH_TWITTER_APP_SECRET
end