TrackAdvisor::Application.routes.draw do
  root :to => "home#index"

  scope "api" do
    resources :users
  end

  match "logout"                  => "sessions#destroy",         :as => :logout, :via => :delete
  match "auth/:provider/redirect" => "authentications#redirect", :as => :auth_provider
  match "auth/facebook/callback"  => "authentications#facebook"
  match "auth/twitter/callback"   => "authentications#twitter"
end
