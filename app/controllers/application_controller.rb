class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user_session
  helper_method :current_user
  helper_method :logged_in?

  def check_logged_in
    unless logged_in?
      store_location
      redirect_to root_path
    end
  end

  def check_logged_out
    redirect_to root_path if logged_in?
  end

  def log_in(user)
    current_user_session.destroy if logged_in?
    UserSession.create(user)
  end

  def current_user_session
    @current_user_session ||= UserSession.find
  end

  def current_user
    @current_user ||= current_user_session && current_user_session.user
  end

  def logged_in?
    current_user.present?
  end

  protected

  def store_location
    session[:return_to] = request.fullpath if request.get?
  end

  def back_or_default(default)
    ((session[:return_to] || params[:return_to]) || default).tap do
      session.delete(:return_to)
    end
  end
end
