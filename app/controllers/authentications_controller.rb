class AuthenticationsController < ApplicationController
  def redirect
    redirect_to "/auth/#{params[:provider]}"
  end

  def facebook
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    authorize
  end

  def twitter
    @user = User.find_for_twitter_oauth(request.env["omniauth.auth"], current_user)
    authorize
  end

  protected

  def authorize
    if @user.persisted?
      log_in(@user)
    end
    redirect_to back_or_default(edit_user_path(@user))
  end
end
