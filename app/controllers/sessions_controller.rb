class SessionsController < ApplicationController
  before_filter :check_logged_in

  def destroy
    current_user_session.destroy
    redirect_to root_path
  end
end
