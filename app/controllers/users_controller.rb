class UsersController < ApplicationController
  before_filter :check_logged_in, :except => [:index, :show]

  def index
    render :json => User.all, :root => false
  end

  def show
    render :json => User.find(params[:id])
  end

  def update
    render :json => User.update(params[:id], params[:user])
  end

  def destroy
    respond :json => User.destroy(params[:id])
  end

  def search
    render :json => User.all, :root => false
  end
end
