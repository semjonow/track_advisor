class UserSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper

  attributes :id, :username, :full_name, :created_at, :updated_at

  def id
    object.slug
  end

  def created_at
   "#{time_ago_in_words(object.created_at)} ago"
  end

  def updated_at
    "#{time_ago_in_words(object.updated_at)} ago"
  end
end
