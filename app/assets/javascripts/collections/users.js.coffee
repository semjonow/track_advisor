class TrackAdvisor.Collections.Users extends Backbone.Collection
  url: "/api/users"
  model: TrackAdvisor.Models.User
