
window.template = (id) ->
  return _.template($("##{id}").html())


class TrackAdvisor.Models.Person extends Backbone.Model
  defaults: {
    age: 18,
    name: "John Smith"
    occupation: "Software Engineer"
  }

  initialize: ->
    this.on("invalid", (model, error)->
      console.log(error)
    )

  validate: (attrs)->
    if attrs.age < 0
      "Age should be positive"
    if !attrs.name
      "Name should be present"

  work: ->
    return this.get("name") + " is worker"

class TrackAdvisor.Collections.PeopleCollection extends Backbone.Collection
  model: TrackAdvisor.Models.Person

class TrackAdvisor.Views.PeopleView extends Backbone.View
  tagName: "ul"

  render: ->
    this.collection.each (person) =>
      @personView = new TrackAdvisor.Views.PersonView(model: person)
      this.$el.append(@personView.render().el)
    return this

class TrackAdvisor.Views.PersonView extends Backbone.View
  tagName: "li"

  initialize: ->
    this.render()

  template: template("personTemplate")

  #template: _.template( $("#personTemplate").html() )

  render: ->
    this.$el.html(this.template(this.model.toJSON()))
    return this

@person = new TrackAdvisor.Models.Person()

@peopleCollection = new TrackAdvisor.Collections.PeopleCollection([
  {
    name: "Johny"
    age:  20
  },
  {
    name: "Sunny",
    age: 30
  }
])
@peopleCollection.add(@person)

@peopleView = new TrackAdvisor.Views.PeopleView(collection: @peopleCollection)

$("body").append(@peopleView.render().el)