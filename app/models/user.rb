class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, :use => :slugged

  include Authentication

  has_many :providers
  has_one  :setting

  attr_accessible :username, :email, :full_name

  validates :email,    :presence => true, :uniqueness => { :case_sensitive => false }
  validates :username, :presence => true, :uniqueness => { :case_sensitive => false }

  before_create :build_setting

  def facebook_client
    return @facebook_client unless @facebook_client.nil?
    unless (user_facebook = facebook).nil?
      @facebook_client = Koala::Facebook::API.new(user_facebook.token)
    end
  end

  def twitter_client
    return @twitter_client unless @twitter_client.nil?
    unless (user_twitter = twitter).nil?
      @twitter_client = Twitter::Client.new(
          :oauth_token        => user_twitter.token,
          :oauth_token_secret => user_twitter.token_secret
      )
    end
  end

  def post_to_twitter(message)
    twitter_client.update(message) if !twitter_client.nil? && setting.post_to_twitter
  end

  def post_to_facebook(message)
    facebook_client.put_connections("me", "feed", :message => message) if !facebook_client.nil? && setting.post_to_facebook
  end

  def facebook
    providers.find_by_name("facebook")
  end

  def twitter
    providers.find_by_name("twitter")
  end

  def self.current
    UserSession.find.user
  end

  protected

  @facebook_client = nil
  @twitter_client  = nil
end
