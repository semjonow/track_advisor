class Provider < ActiveRecord::Base
  belongs_to :user, :touch => true

  attr_accessible :name, :uid, :token, :token_secret

  validates :uid,  :presence => true, :uniqueness => true
  validates :name, :presence => true
end