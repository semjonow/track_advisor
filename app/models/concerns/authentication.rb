require 'active_support/concern'

module Authentication
  extend ActiveSupport::Concern
  included do
    class_eval do

      acts_as_authentic do |config|
        config.validate_email_field    = false
        config.validate_password_field = false
      end

      def self.find_for_facebook_oauth(auth, signed_in_resource = nil)
        params = {
            :provider_name => auth.provider,
            :uid           => auth.uid,
            :email         => auth.info.email,
            :username      => auth.info.nickname,
            :full_name     => auth.info.name,
            :token         => auth.credentials.token,
            :token_secret  => ""
        }
        User.find_or_create_by(params, signed_in_resource)
      end

      def self.find_for_twitter_oauth(auth, signed_in_resource = nil)
        params = {
            :provider_name => auth.provider,
            :uid           => auth.uid,
            :username      => auth.info.nickname,
            :full_name     => auth.info.name,
            :token         => auth.credentials.token,
            :token_secret  => auth.credentials.secret
        }
        User.find_or_create_by(params, signed_in_resource)
      end

      protected

      def self.find_or_create_by(params = {}, signed_in_resource = nil)
        provider = Provider.where(:name => params[:provider_name], :uid => params[:uid]).first

        unless provider
          user = params[:email].nil? ? nil : User.find_by_email(params[:email])

          if user.nil?
            if signed_in_resource.nil?
              user = User.new(
                  :username  => User.generate_random_username(params[:username]),
                  :email     => params[:email].nil? ? User.generate_random_email : params[:email],
                  :full_name => params[:full_name].nil? ? "" : params[:full_name]
              )
              user.save!
            else
              user = signed_in_resource
            end
          end

          similar_provider = user.providers.where(:name => params[:provider_name]).first
          if similar_provider.nil?
            user.providers.create!(
                :name         => params[:provider_name],
                :uid          => params[:uid],
                :token        => params[:token],
                :token_secret => params[:token_secret]
            )
          else
            similar_provider.update_attributes!(
                :uid          => params[:uid],
                :token        => params[:token],
                :token_secret => params[:token_secret]
            )
          end
        else
          user = provider.user
        end
        user
      end

      def self.generate_random_email
        random_email = "user.#{Random.rand(99999999)}@trackadvisor.co"
        return random_email unless User.exists?(:email => random_email)
        User.generate_random_email
      end

      def self.generate_random_username(username = nil)
        random_username = username || "user.#{Random.rand(99999999)}"
        return random_username unless User.exists?(:username => random_username)
        User.generate_random_username(nil)
      end
    end
  end
end