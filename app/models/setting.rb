class Setting < ActiveRecord::Base
  belongs_to :user, :touch => true

  attr_accessible :post_to_facebook, :post_to_twitter
end
