class CreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      t.string :email,             :null => false, :default => ""
      t.string :username,          :null => false, :default => ""
      t.string :full_name,         :null => false, :default => ""
      t.string :slug,              :null => false, :default => ""
      t.string :crypted_password,  :null => false, :default => ""
      t.string :password_salt,     :null => false, :default => ""
      t.string :persistence_token, :null => false, :default => ""
      t.string :perishable_token,  :null => false, :default => ""
      t.timestamps
    end

    add_index :users, :email,    :unique => true
    add_index :users, :username, :unique => true
    add_index :users, :slug,     :unique => true
  end
end
