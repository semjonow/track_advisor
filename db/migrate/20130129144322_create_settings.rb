class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :user_id,          :null => false
      t.boolean :post_to_facebook, :null => false, :default => false
      t.boolean :post_to_twitter,  :null => false, :default => false

      t.timestamps
    end

    add_foreign_key(:settings, :users, :dependent => :delete)
  end
end